# OpenML dataset: kin8nm

https://www.openml.org/d/44980

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

A realistic simulation of the forward dynamics of an 8 link all-revolute robot arm. The task in all datasets is to predict the distance of the end-effector from a target. The input are the angular positions of the joints. The task is medium noisy and nonlinear.

Each instance represents a configuration of angular positions of the joints and the resulting distance of the end-effector from a target.

**Attribute Description**

1. *theta[1-8]* - angular positions of the joints
2. *y* - resulting distance of end-effector from target, target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44980) of an [OpenML dataset](https://www.openml.org/d/44980). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44980/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44980/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44980/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

